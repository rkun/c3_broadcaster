#!/bin/bash

WEBCAM_INPUT=/dev/video0
VIDEO_SIZE=1280x720
PULSEAUDIO_INPUT=default
YOUTUBE_URL=$1
YOUTUBE_BACKUP_URL=$2
YOUTUBE_SECRET=$3

ffmpeg -f pulse -i $PULSEAUDIO_INPUT -f v4l2 -i $WEBCAM_INPUT -c:v libx264 -x264-params keyint=120:scenecut=0 -b:v 4000k -r 30 -video_size $VIDEO_SIZE -c:a aac -b:a 128k -ar 44100 -f flv pipe: | ffmpeg -re -i - -c:v copy -f flv "$YOUTUBE_URL/$YOUTUBE_SECRET" -codec copy -f flv "$YOUTUBE_BACKUP_URL/$YOUTUBE_SECRET"
